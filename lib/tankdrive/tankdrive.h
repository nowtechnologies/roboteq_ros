#ifndef TANKDRIVE_H
#define TANKDRIVE_H
#include "Arduino.h"

class Tank{
public:
  struct Power{
    float left;
    float right;
  };
  Power power;

  struct Power drive(float throttle, float steer);

};
#endif
