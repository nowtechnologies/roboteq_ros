#include "tankdrive.h"

#define GYEBRO_DRIVE

// M

#ifdef SQUARE_DRIVE
Tank::Power Tank::drive(float y, float x){

  Power power;
  //convert to polar
  float r = hypot(x, y);
  float t = atan2(y, x);
  //rotate by 45 degrees
  t += PI / 4;
  //back to cartesian
  power.left = (r * cos(t))*sqrt(2);
  power.right = (r * sin(t))*sqrt(2);
  //constrain
  power.left = constrain(power.left, -1.0f, 1.0f);
  power.right = constrain(power.right, -1.0f, 1.0f);
  return power;
}
#endif

#ifdef GYEBRO_DRIVE
// GYEBRO

float sgn(float in){
  float out;
    if (in > 0) out = 1.0f;
    else out = -1.0f;
  return out;
}

const float TF=0.5f;
Tank::Power Tank::drive(float yFwd, float xR) {
    Power power;
    float forward  = TF*yFwd;
    float turnQuad = (1.0f-TF)*xR*xR*sgn(xR);
    power.right  =  (forward + turnQuad);
    power.left   = -(forward - turnQuad);
    return power;
}
#endif

#ifdef OMNIPOLAR_DRIVE
// OMNIPOLAR

const float DL = 0.50f; // drive limit
const float TF = 0.10f; // turn factor

Tank::Power Tank::drive(float D, float T) {

    D *= DL;
    float O = TF/DL;
    float v = (1.0f - fabs(D))*T + T;
    float w = (1.0f - fabs(T))*D + D;
    float L = (v - w)/2.0f;
    float R = (v + w)/2.0f;
    float coeff = fabs(T)*fabs(D)*(1 - O);
    if ((T > 0) && (D > 0)) {
        L+=coeff;
    } else if ((T < 0) && (D < 0)) {
        L-=coeff;
    }
    if ((T > 0) && (D < 0)) {
        R+=coeff;
    } else if ((T < 0) && (D > 0)) {
        R-=coeff;
    }
    float maxMotorScale = max(1, max(fabs(R), fabs(L)));
    L /= maxMotorScale;
    R /= maxMotorScale;
    Power power;
    power.left =  L;
    power.right = R;

    return power;
}
#endif
