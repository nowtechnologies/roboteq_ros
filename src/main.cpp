#define USE_USBCON
#include <ros.h>
#include <Arduino.h>
#include <RobotEQ.h>
#include <tankdrive.h>
#include <geometry_msgs/Twist.h>

Tank tank;
Tank::Power motor;
#define maxpower 1000

RobotEQ chair(&Serial1);
bool chair_connected = false;

ros::NodeHandle nh;
bool got_new_cmd = false;

void cmd_cb( const geometry_msgs::Twist& twist ){
  // throttle , steer
  motor = tank.drive(-twist.linear.x, twist.angular.z);
  got_new_cmd = true;
}

ros::Subscriber<geometry_msgs::Twist> sub("/cmd_vel", &cmd_cb );

float mapfloat(float x, float in_min, float in_max, float out_min, float out_max)
{ return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min; }

void setup(){

  Serial1.begin(9600);
  chair.powerOff(false);
	delay(250);
	int retryCounter = 0;
	bool connect_success = true;
  while (chair.isConnected() != ROBOTEQ_OK){
		if (retryCounter > 3) {
			connect_success = false;
    break;
		}
		delay(100);
		retryCounter++;
	}
	if (connect_success){
		chair.commandMotorPower(1, 0);
		chair.commandMotorPower(2, 0);
		chair_connected = true;
	} else {
		chair_connected = false;
	}

  nh.initNode();
  nh.subscribe(sub);
}

void loop(){
  nh.spinOnce();
  if (got_new_cmd) {
    chair.commandMotorPower( 1, int( mapfloat( motor.left , -1.0f, 1.0f, maxpower, -maxpower) ) );
    chair.commandMotorPower( 2, int( mapfloat( motor.right, -1.0f, 1.0f, maxpower, -maxpower) ) );
    got_new_cmd = false;
  }
}
